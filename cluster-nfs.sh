#!/bin/bash

NET="cluster-nfs-net"
SVC="cluster-nfs"
docker network rm "${NET}"
docker network create "${NET}" --internal --attachable -d overlay --opt encrypted --scope swarm
/usr/bin/docker kill "${SVC}"
/usr/bin/docker run --name "${SVC}" \
  --restart=always \
  --privileged \
  -e NFS_DISABLE_VERSION_3=1 \
  --network "${NET}" \
  --network-alias "${SVC}" \
  -v /opt/srv:/opt/srv \
  -v /etc/exports:/etc/exports:z \
  erichough/nfs-server

#  -p 2049:2049 \
