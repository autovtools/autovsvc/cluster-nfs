#!/bin/bash

# Redirect 127.0.0.1:2049 -> cluster-nfs:2049
# Each node's localhost:2049 reaches the cluster-nfs share over an encrypted overlay,
# But no external hosts can directly connect to it

NET="cluster-nfs-net"
SVC="cluster-nfs-proxy"
/usr/bin/docker kill "${SVC}"
/usr/bin/docker rm "${SVC}"
/usr/bin/docker create --name "${SVC}" \
  --restart=always \
  -p "127.0.0.1:2049:2049" \
  -v /etc/haproxy.${SVC}.cfg:/usr/local/etc/haproxy/haproxy.cfg:z \
  haproxy:alpine

/usr/bin/docker network connect "${NET}" "${SVC}"
/usr/bin/docker start "${SVC}"

